/*
 * relative-time.js
 *   Replaces the timestamps in posts to show 'x minutes/hours/days ago',
 *   while displaying the absolute time in tooltip 
 *
 * Usage:
 *   $config['additional_javascript'][] = 'js/jquery.min.js';
 *   $config['additional_javascript'][] = 'js/relative-time.js';
 */
if (active_page == 'index' || active_page == 'thread') {
	onready(function () {
		'use strict';

		function update() {
			var currentTime = Date.now();
			$('div.post time').each(function () {
				var postTime = new Date($(this).attr('datetime'));

				$(this)
					.text( timeDifference(currentTime, postTime.getTime()) )
					.attr('title', postTime.toLocaleString('sk-SK', {
						weekday: 'short',
						day: 'numeric',
						month: 'numeric',
						year: 'numeric',
						hour: 'numeric',
						minute: 'numeric',
						second: 'numeric',
						hour12: false
					}));
			});
		}
		
		function timeDifference(current, previous) {

			var msPerMinute = 60 * 1000;
			var msPerHour = msPerMinute * 60;
			var msPerDay = msPerHour * 24;
			var msPerMonth = msPerDay * 30;
			var msPerYear = msPerDay * 365;

			var elapsed = current - previous;

			if (elapsed < msPerMinute) {
				return 'teraz';
			} else if ((elapsed + msPerMinute/2) < msPerHour) {
				return 'pred ' + (Math.round(elapsed/msPerMinute)<=1 ? 'minútou' : Math.round(elapsed/msPerMinute) + ' minútami');
			} else if ((elapsed + msPerHour/2) < msPerDay) {
				return 'pred ' + (Math.round(elapsed/msPerHour)<=1 ? 'hodinou' : Math.round(elapsed/msPerHour) + ' hodinami');
			} else if ((elapsed + msPerDay/2) < msPerMonth) {
				return Math.round(elapsed/msPerDay)<=1 ? 'včera' : 'pred ' + Math.round(elapsed/msPerDay) + ' dňami';
			} else if ((elapsed + msPerMonth/2) < msPerYear) {
				return 'pred ' + (Math.round(elapsed/msPerMonth)<=1 ? 'mesiacom' : Math.round(elapsed/msPerMonth) + ' mesiacmi');
			} else {
				return 'pred ' + (Math.round(elapsed/msPerYear)<=1 ? 'rokom' : Math.round(elapsed/msPerYear) + ' rokmi');
			}
		}
			setInterval(update, 30000);
			$(document).on('new_post', update);
			update();
	});
}
