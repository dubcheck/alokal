<?php
/*
 *  Copyright (c) 2010-2014 Tinyboard Development Group
 */

require 'inc/functions.php';
require 'inc/anti-bot.php';

// Fix for magic quotes
if (get_magic_quotes_gpc()) {
	function strip_array($var) {
		return is_array($var) ? array_map('strip_array', $var) : stripslashes($var);
	}
	
	$_GET = strip_array($_GET);
	$_POST = strip_array($_POST);
}

if (isset($_POST['delete'])) {
	// Delete
	
	if (!isset($_POST['board'], $_POST['password']))
		error($config['error']['bot']);
	
	$password = &$_POST['password'];
	
	if ($password == '')
		error($config['error']['invalidpassword']);
	
	$delete = array();
	foreach ($_POST as $post => $value) {
		if (preg_match('/^delete_(\d+)$/', $post, $m)) {
			$delete[] = (int)$m[1];
		}
	}
	
	checkDNSBL();
		
	// Check if board exists
	if (!openBoard($_POST['board']))
		error($config['error']['noboard']);
	
	// Check if banned
	checkBan($board['uri']);

	// Check if deletion enabled
	if (!$config['allow_delete'])
		error(_('Post deletion is not allowed!'));
	
	if (empty($delete))
		error($config['error']['nodelete']);
		
	foreach ($delete as &$id) {
		$query = prepare(sprintf("SELECT `thread`, `time`,`password` FROM ``posts_%s`` WHERE `id` = :id", $board['uri']));
		$query->bindValue(':id', $id, PDO::PARAM_INT);
		$query->execute() or error(db_error($query));
		
		if ($post = $query->fetch(PDO::FETCH_ASSOC)) {
			$thread = false;
			if ($config['user_moderation'] && $post['thread']) {
				$thread_query = prepare(sprintf("SELECT `time`,`password` FROM ``posts_%s`` WHERE `id` = :id", $board['uri']));
				$thread_query->bindValue(':id', $post['thread'], PDO::PARAM_INT);
				$thread_query->execute() or error(db_error($query));

				$thread = $thread_query->fetch(PDO::FETCH_ASSOC);	
			}

			if ($password != '' && $post['password'] != $password && (!$thread || $thread['password'] != $password))
				error($config['error']['invalidpassword']);
			
			if ($post['time'] > time() - $config['delete_time'] && (!$thread || $thread['password'] != $password)) {
				error(sprintf($config['error']['delete_too_soon'], until($post['time'] + $config['delete_time'])));
			}
			
			if (isset($_POST['file'])) {
				// Delete just the file
				deleteFile($id);
			} else {
				// Delete entire post
				deletePost($id);
			}
			
			_syslog(LOG_INFO, 'Deleted post: ' .
				'/' . $board['dir'] . $config['dir']['res'] . sprintf($config['file_page'], $post['thread'] ? $post['thread'] : $id) . ($post['thread'] ? '#' . $id : '')
			);
		}
	}
	
	buildIndex();


	rebuildThemes('post-delete', $board['uri']);
	
	$is_mod = isset($_POST['mod']) && $_POST['mod'];
	$root = $is_mod ? $config['root'] . $config['file_mod'] . '?/' : $config['root'];
	
	if (!isset($_POST['json_response'])) {
		header('Location: ' . $root . $board['dir'] . $config['link_index'], true, $config['redirect_http']);
	} else {
		header('Content-Type: text/json');
		echo json_encode(array('success' => true));
	}
} elseif (isset($_POST['report'])) {
	if (!isset($_POST['board'], $_POST['reason']))
		error($config['error']['bot']);
	
	$report = array();
	foreach ($_POST as $post => $value) {
		if (preg_match('/^delete_(\d+)$/', $post, $m)) {
			$report[] = (int)$m[1];
		}
	}
	
	checkDNSBL();
		
	// Check if board exists
	if (!openBoard($_POST['board']))
		error($config['error']['noboard']);
	
	// Check if banned
	checkBan($board['uri']);
	
	if (empty($report))
		error($config['error']['noreport']);
	
	if (count($report) > $config['report_limit'])
		error($config['error']['toomanyreports']);
	
	$reason = escape_markup_modifiers($_POST['reason']);
	markup($reason);
	
	foreach ($report as &$id) {
		$query = prepare(sprintf("SELECT `thread` FROM ``posts_%s`` WHERE `id` = :id", $board['uri']));
		$query->bindValue(':id', $id, PDO::PARAM_INT);
		$query->execute() or error(db_error($query));
		
		$thread = $query->fetchColumn();
		
		if ($config['syslog'])
			_syslog(LOG_INFO, 'Reported post: ' .
				'/' . $board['dir'] . $config['dir']['res'] . sprintf($config['file_page'], $thread ? $thread : $id) . ($thread ? '#' . $id : '') .
				' for "' . $reason . '"'
			);
		$query = prepare("INSERT INTO ``reports`` VALUES (NULL, :time, :ip, :board, :post, :reason)");
		$query->bindValue(':time', time(), PDO::PARAM_INT);
		$query->bindValue(':ip', $_SERVER['REMOTE_ADDR'], PDO::PARAM_STR);
		$query->bindValue(':board', $board['uri'], PDO::PARAM_INT);
		$query->bindValue(':post', $id, PDO::PARAM_INT);
		$query->bindValue(':reason', $reason, PDO::PARAM_STR);
		$query->execute() or error(db_error($query));
	}
	
	$is_mod = isset($_POST['mod']) && $_POST['mod'];
	$root = $is_mod ? $config['root'] . $config['file_mod'] . '?/' : $config['root'];
	
	if (!isset($_POST['json_response'])) {
		header('Location: ' . $root . $board['dir'] . $config['link_index'], true, $config['redirect_http']);
	} else {
		header('Content-Type: text/json');
		echo json_encode(array('success' => true));
	}
} elseif (isset($_POST['post'])) {
	if (!isset($_POST['body'], $_POST['board']))
		error($config['error']['bot']);

	$post = array('board' => $_POST['board'], 'files' => array());

	// Check if board exists
	if (!openBoard($post['board']))
		error($config['error']['noboard']);
	
	if (!isset($_POST['name']))
		$_POST['name'] = $config['anonymous'];
	
	if (!isset($_POST['email']))
		$_POST['email'] = '';
	
	if (!isset($_POST['subject']))
		$_POST['subject'] = '';
	
	if (!isset($_POST['password']))
		$_POST['password'] = '';	
	
	if (isset($_POST['thread'])) {
		$post['op'] = false;
		$post['thread'] = round($_POST['thread']);
	} else
		$post['op'] = true;

	if (!(($post['op'] && $_POST['post'] == $config['button_newtopic']) ||
		(!$post['op'] && $_POST['post'] == $config['button_reply'])))
		error($config['error']['bot']);
	
	// Check the referrer
	if ($config['referer_match'] !== false &&
		(!isset($_SERVER['HTTP_REFERER']) || !preg_match($config['referer_match'], rawurldecode($_SERVER['HTTP_REFERER']))))
		error($config['error']['referer']);
	
	checkDNSBL();
		
	// Check if banned
	checkBan($board['uri']);
	
	// Check for CAPTCHA right after opening the board so the "return" link is in there
	if ($config['recaptcha']) {
		if (!isset($_POST['recaptcha_challenge_field']) || !isset($_POST['recaptcha_response_field']))
			error($config['error']['bot']);
		// Check what reCAPTCHA has to say...
		$resp = recaptcha_check_answer($config['recaptcha_private'],
			$_SERVER['REMOTE_ADDR'],
			$_POST['recaptcha_challenge_field'],
			$_POST['recaptcha_response_field']);
		if (!$resp->is_valid) {
			error($config['error']['captcha']);
		}
	}
	
	if ($post['mod'] = isset($_POST['mod']) && $_POST['mod']) {
		require 'inc/mod/auth.php';
		if (!$mod) {
			// Liar. You're not a mod.
			error($config['error']['notamod']);
		}
		
		$post['sticky'] = $post['op'] && isset($_POST['sticky']);
		$post['locked'] = $post['op'] && isset($_POST['lock']);
		$post['raw'] = isset($_POST['raw']);
		
		if ($post['sticky'] && !hasPermission($config['mod']['sticky'], $board['uri']))
			error($config['error']['noaccess']);
		if ($post['locked'] && !hasPermission($config['mod']['lock'], $board['uri']))
			error($config['error']['noaccess']);
		if ($post['raw'] && !hasPermission($config['mod']['rawhtml'], $board['uri']))
			error($config['error']['noaccess']);
	}
	
	// if (!$post['mod']) {
		// $post['antispam_hash'] = checkSpam(array($board['uri'], isset($post['thread']) ? $post['thread'] : ($config['try_smarter'] && isset($_POST['page']) ? 0 - (int)$_POST['page'] : null)));
		// if ($post['antispam_hash'] === true)
			// error($config['error']['spam']);
	// }
	
	if ($config['robot_enable'] && $config['robot_mute']) {
		checkMute();
	}
	
	//Check if thread exists
	if (!$post['op']) {
		$query = prepare(sprintf("SELECT `sticky`,`locked`,`sage` FROM ``posts_%s`` WHERE `id` = :id AND `thread` IS NULL LIMIT 1", $board['uri']));
		$query->bindValue(':id', $post['thread'], PDO::PARAM_INT);
		$query->execute() or error(db_error());
		
		if (!$thread = $query->fetch(PDO::FETCH_ASSOC)) {
			// Non-existant
			error($config['error']['nonexistant']);
		}
	}
		
	
	// Check for an embed field
	if (($config['enable_embedding'] || $config['allow_upload_by_url'])	&& isset($_POST['remote_url']) && !empty($_POST['remote_url'])) {
		if ($config['enable_embedding']) {
			// yep; validate it
			$value = $_POST['remote_url'];
			foreach ($config['embedding'] as &$embed) {
				if (preg_match($embed[0], $value)) {
					// Valid link
					$post['embed'] = $value;
					// This is bad, lol.
					$post['no_longer_require_an_image_for_op'] = true;
					break;
				}
			}
		}
		if (!isset($post['embed'])) {
			if ($config['allow_upload_by_url']) {
				$post['file_url'] = $_POST['remote_url'];
				if (!preg_match('@^https?://@', $post['file_url']))
					error($config['error']['invalidimg']);
		
				if (mb_strpos($post['file_url'], '?') !== false)
					$url_without_params = mb_substr($post['file_url'], 0, mb_strpos($post['file_url'], '?'));
				else
					$url_without_params = $post['file_url'];

				$post['extension'] = strtolower(mb_substr($url_without_params, mb_strrpos($url_without_params, '.') + 1));
				if (!in_array($post['extension'], $config['allowed_images']) && !in_array($post['extension'], $config['allowed_ext_files']))
					error($config['error']['unknownext']);

				$post['file_tmp'] = tempnam($config['tmp'], 'url');
				function unlink_tmp_file($file) {
					@unlink($file);
					fatal_error_handler();
				}
				register_shutdown_function('unlink_tmp_file', $post['file_tmp']);
		
				$fp = fopen($post['file_tmp'], 'w');
		
				$curl = curl_init();
				curl_setopt($curl, CURLOPT_URL, $post['file_url']);
				curl_setopt($curl, CURLOPT_FAILONERROR, true);
				curl_setopt($curl, CURLOPT_FOLLOWLOCATION, false);
				curl_setopt($curl, CURLOPT_CONNECTTIMEOUT, 5);
				curl_setopt($curl, CURLOPT_TIMEOUT, $config['upload_by_url_timeout']);
				curl_setopt($curl, CURLOPT_USERAGENT, 'Tinyboard');
				curl_setopt($curl, CURLOPT_BINARYTRANSFER, true);
				curl_setopt($curl, CURLOPT_FILE, $fp);
				curl_setopt($curl, CURLOPT_PROTOCOLS, CURLPROTO_HTTP | CURLPROTO_HTTPS);
		
				if (curl_exec($curl) === false)
					error($config['error']['nomove'] . '<br/>Curl says: ' . curl_error($curl));
		
				curl_close($curl);
		
				fclose($fp);

				$_FILES['file'] = array(
					'name' => basename($url_without_params),
					'tmp_name' => $post['file_tmp'],
					'file_tmp' => true,
					'error' => 0,
					'size' => filesize($post['file_tmp'])
				);
			}
			else
				error($config['error']['invalid_embed']);
		}
	}
	
	if (!hasPermission($config['mod']['bypass_field_disable'], $board['uri'])) {
		if ($config['field_disable_name'])
			$_POST['name'] = $config['anonymous']; // "forced anonymous"
	
		if ($config['field_disable_email'])
			$_POST['email'] = '';
	
		if ($config['field_disable_password'])
			$_POST['password'] = '';
	
		if ($config['field_disable_subject'] || (!$post['op'] && $config['field_disable_reply_subject']))
			$_POST['subject'] = '';
	}

	$post['name'] = $_POST['name'] != '' ? $_POST['name'] : $config['anonymous'];
	$post['subject'] = $_POST['subject'];
	$post['email'] = str_replace(' ', '%20', htmlspecialchars($_POST['email']));
	$post['body'] = $_POST['body'];
	$post['password'] = $_POST['password'];
	$post['has_file'] = (!isset($post['embed']) && (($post['op'] && !isset($post['no_longer_require_an_image_for_op']) && $config['force_image_op']) || !empty($_FILES['file']['name'])));
	
	if (!($post['has_file'] || isset($post['embed'])) || (($post['op'] && $config['force_body_op']) || (!$post['op'] && $config['force_body']))) {
		$stripped_whitespace = preg_replace('/[\s]/u', '', $post['body']);
		if ($stripped_whitespace == '') {
			error($config['error']['tooshort_body']);
		}
	}
	
	if (!$post['op']) {
		// Check if thread is locked
		// but allow mods to post
		if ($thread['locked'] && !hasPermission($config['mod']['postinlocked'], $board['uri']))
			error($config['error']['locked']);
		
		$numposts = numPosts($post['thread']);
		
		if ($config['reply_hard_limit'] != 0 && $config['reply_hard_limit'] <= $numposts['replies'])
			error($config['error']['reply_hard_limit']);
		
		if ($post['has_file'] && $config['image_hard_limit'] != 0 && $config['image_hard_limit'] <= $numposts['images'])
			error($config['error']['image_hard_limit']);
	}
		
	if ($post['has_file']) {
		// Determine size sanity
		$size = 0;
		if ($config['multiimage_method'] == 'split') {
			foreach ($_FILES as $key => $file) {
				$size += $file['size'];
			}
		} elseif ($config['multiimage_method'] == 'each') {
			foreach ($_FILES as $key => $file) {
				if ($file['size'] > $size) {
					$size = $file['size'];
				}
			}
		} else {
			error(_('Unrecognized file size determination method.'));
		}

		if ($size > $config['max_filesize'])
			error(sprintf3($config['error']['filesize'], array(
				'sz' => number_format($size),
				'filesz' => number_format($size),
				'maxsz' => number_format($config['max_filesize'])
			)));
		$post['filesize'] = $size;
	}
	
	
	$post['capcode'] = false;
	
	if ($mod && preg_match('/^((.+) )?## (.+)$/', $post['name'], $matches)) {
		$name = $matches[2] != '' ? $matches[2] : $config['anonymous'];
		$cap = $matches[3];
		
		if (isset($config['mod']['capcode'][$mod['type']])) {
			if (	$config['mod']['capcode'][$mod['type']] === true ||
				(is_array($config['mod']['capcode'][$mod['type']]) &&
					in_array($cap, $config['mod']['capcode'][$mod['type']])
				)) {
				
				$post['capcode'] = utf8tohtml($cap);
				$post['name'] = $name;
			}
		}
	}
	
	$trip = generate_tripcode($post['name']);
	$post['name'] = $trip[0];
	$post['trip'] = isset($trip[1]) ? $trip[1] : '';
	
	$noko = false;
	if (strtolower($post['email']) == 'noko') {
		$noko = true;
		$post['email'] = '';
	} elseif (strtolower($post['email']) == 'nonoko'){
		$noko = false;
		$post['email'] = '';
	} else $noko = $config['always_noko'];
		
	if ($post['has_file']) {
		foreach ($_FILES as $key => $file) {
			if ($file['size'] && $file['tmp_name']) {
				$file['filename'] = urldecode(get_magic_quotes_gpc() ? stripslashes($file['name']) : $file['name']);
				$file['extension'] = strtolower(mb_substr($file['filename'], mb_strrpos($file['filename'], '.') + 1));
				if ($file['extension'] == 'jpeg')
					$file['extension'] = 'jpg';
				if ($file['extension'] == 'aac')
					$file['extension'] = 'm4a';
				if ($file['extension'] == 'm4v')
					$file['extension'] = 'mp4';
				// if ($file['extension'] == 'gif') {
					// exec("ffprobe -v quiet -print_format json -show_streams -count_frames " . escapeshellarg($file['tmp_name']), $ffprobe_gif);
					// $ffprobe_gif = json_decode(implode("\n", $ffprobe_gif), 1);
					// if ($ffprobe_gif['streams'][0]["nb_read_frames"] > 1) {
						// if ($error = shell_exec_error("ffmpeg -i " . escapeshellarg($file['tmp_name']) . " -c:v libvpx -quality best -cpu-used 0 -qmin 0 -qmax 63 -crf 21 -b:v 0 -pix_fmt yuva420p -pass 1 -v error -y " . escapeshellarg($file['tmp_name']) . ".webm"))
							// error($error);
						// if ($error = shell_exec_error("ffmpeg -i " . escapeshellarg($file['tmp_name']) . " -c:v libvpx -quality best -cpu-used 0 -qmin 0 -qmax 63 -crf 21 -b:v 0 -pix_fmt yuva420p -pass 2 -v error -y " . escapeshellarg($file['tmp_name']) . ".webm"))
							// error($error);
						// rename($file['tmp_name'] . ".webm", $file['tmp_name']);
						// $file['extension'] = 'webm';
					// }
					// else {
						// if ($error = shell_exec_error("convert " . escapeshellarg($file['tmp_name']) . " " . escapeshellarg($file['tmp_name']) . ".png"))
							// error($error);
						// rename($file['tmp_name'] . ".png", $file['tmp_name']);
						// $file['extension'] = 'png';
					// }
				// }
				$file['filename'] = mb_substr($file['filename'], 0, mb_strrpos($file['filename'], '.') + 1) . $file['extension'];
				if (isset($config['filename_func']))
					$file['file_id'] = $config['filename_func']($file);
				else
					$file['file_id'] = gmp_strval(gmp_init(hash_file('crc32', $file['tmp_name']), 16), 62);
				
				$file['file'] = $config['dir']['img'] . $file['file_id'] . '.' . $file['extension'];
				$file['thumb'] = $config['dir']['thumb'] . $file['file_id'] . '.' . ($config['thumb_ext'] ? $config['thumb_ext'] : $file['extension']);
				$post['files'][] = $file;
			}
		}
	}

	if (empty($post['files'])) $post['has_file'] = false;

	// Check for a file
	if ($post['op'] && !isset($post['no_longer_require_an_image_for_op'])) {
		if (!$post['has_file'] && $config['force_image_op'])
			error($config['error']['noimage']);
	}

	// Check for too many files
	if (sizeof($post['files']) > $config['max_images'])
		error($config['error']['toomanyimages']);

	if ($config['strip_combining_chars']) {
		$post['name'] = strip_combining_chars($post['name']);
		$post['email'] = strip_combining_chars($post['email']);
		$post['subject'] = strip_combining_chars($post['subject']);
		$post['body'] = strip_combining_chars($post['body']);
	}
	
	// Check string lengths
	if (mb_strlen($post['name']) > 35)
		error(sprintf($config['error']['toolong'], 'name'));	
	if (mb_strlen($post['email']) > 40)
		error(sprintf($config['error']['toolong'], 'email'));
	if (mb_strlen($post['subject']) > 100)
		error(sprintf($config['error']['toolong'], 'subject'));
	if (!$mod && mb_strlen($post['body']) > $config['max_body'])
		error($config['error']['toolong_body']);
	if (mb_strlen($post['password']) > 20)
		error(sprintf($config['error']['toolong'], 'password'));
		
	wordfilters($post['body']);
	
	$post['body'] = escape_markup_modifiers($post['body']);
	
	if ($mod && isset($post['raw']) && $post['raw']) {
		$post['body'] .= "\n<tinyboard raw html>1</tinyboard>";
	}
	
	if ($config['country_flags']) {
		if (!in_array($country_code, array('eu', 'ap', 'o1', 'a1', 'a2')))
			$post['body'] .= "\n<tinyboard flag>".$country_code."</tinyboard>\n<tinyboard flag alt>".$country_name."</tinyboard>";
	}
	if ($config['country_slav']) {
		$slavshits = array('ru', 'ua', 'by', 'pl', 'cz', 'sk', 'si', 'hr', 'rs', 'ba', 'bl', 'me', 'mk');
		$mongols = array('fi', 'hu', 'ee', 'mn', 'cn', 'jp');
		if (in_array($country_code, $slavshits))
			$post['body'] .= "\n<tinyboard flag>slav</tinyboard>\n<tinyboard flag alt>Slovan</tinyboard>";
		elseif (in_array($country_code, $mongols))
			$post['body'] .= "\n<tinyboard flag>mn</tinyboard>\n<tinyboard flag alt>Mongol</tinyboard>";
		else
			$post['body'] .= "\n<tinyboard flag>ger</tinyboard>\n<tinyboard flag alt>Germán</tinyboard>";
	}
	if ($config['rate'] && (strtolower($post['email']) == 'rate' || strtolower($post['email']) == 'rejt')) {
		$post['body'] .= "\n<tinyboard rate>" . $config['rates'][array_rand($config['rates'])] . "</tinyboard>";
		$post['email'] = '';
	}
	if ($config['user_flag'] && isset($_POST['user_flag']))
	if (!empty($_POST['user_flag']) ){
		
		$user_flag = $_POST['user_flag'];
		
		if (!isset($config['user_flags'][$user_flag]))
			error(_('Invalid flag selection!'));

		$flag_alt = isset($user_flag_alt) ? $user_flag_alt : $config['user_flags'][$user_flag];

		$post['body'] .= "\n<tinyboard flag>" . strtolower($user_flag) . "</tinyboard>" .
		"\n<tinyboard flag alt>" . $flag_alt . "</tinyboard>";
	}
	if (isset($post['embed']) && preg_match('/^https?:\/\/(\w+\.)?(?:youtube\.com\/watch\?v=|youtu\.be\/)([a-zA-Z0-9\-_]{10,11})(&.+)?$/i', $post['embed'], $matches)) {
		$data = json_decode(file_get_contents('https://www.googleapis.com/youtube/v3/videos?id=' . $matches[2] . '&key=AIzaSyCfzwvSy7CUL96SiMbtu5a8xVe9rH_Tm2U&part=snippet,contentDetails&fields=items%28snippet%28title,publishedAt,channelTitle%29,contentDetails%28duration%29%29'), true);
		$duration = new DateInterval($data['items']['0']['contentDetails']['duration']);
		$publishedAt = new DateTime($data['items']['0']['snippet']['publishedAt']);
		$post['body'] .= "\n<tinyboard yt_title>" . $data['items']['0']['snippet']['title'] . "</tinyboard>" .
						 "\n<tinyboard yt_duration>" . $duration->format('%H:%I:%S') . "</tinyboard>" .
						 "\n<tinyboard yt_channelTitle>" . $data['items']['0']['snippet']['channelTitle'] . "</tinyboard>" .
						 "\n<tinyboard yt_publishedAt>" . $publishedAt->format('d.m.Y H:i:s') . "</tinyboard>";
	}
	if (mysql_version() >= 50503) {
		$post['body_nomarkup'] = $post['body']; // Assume we're using the utf8mb4 charset
	} else {
		// MySQL's `utf8` charset only supports up to 3-byte symbols
		// Remove anything >= 0x010000
		
		$chars = preg_split('//u', $post['body'], -1, PREG_SPLIT_NO_EMPTY);
		$post['body_nomarkup'] = '';
		foreach ($chars as $char) {
			$o = 0;
			$ord = ordutf8($char, $o);
			if ($ord >= 0x010000)
				continue;
			$post['body_nomarkup'] .= $char;
		}
	}
	
	$post['tracked_cites'] = markup($post['body'], true);

	
	
	if ($post['has_file']) {
		foreach ($post['files'] as $key => &$file) {
			if (!in_array($file['extension'], $config['allowed_images']) && !in_array($file['extension'], $config['allowed_audio']) && !in_array($file['extension'], $config['allowed_videos']) && !in_array($file['extension'], $config['allowed_ext_files']))
				error($config['error']['unknownext']);
			
			$file['is_an_image'] = in_array($file['extension'], $config['allowed_images']);
			$file['is_an_audio'] = in_array($file['extension'], $config['allowed_audio']);
			$file['is_a_video'] = in_array($file['extension'], $config['allowed_videos']);
			
			// Truncate filename if it is too long
			$file['filename'] = mb_substr($file['filename'], 0, $config['max_filename_len']);
			
			if (!isset($filenames)) {
				$filenames = escapeshellarg($file['tmp_name']);
			} else {
				$filenames .= (' ' . escapeshellarg($file['tmp_name']));
			}
			
			if (!is_readable($file['tmp_name']))
				error($config['error']['nomove']);
		}
		if ($output = shell_exec_error("cat $filenames | md5sum")) {
			$explodedvar = explode(' ', $output);
			$hash = $explodedvar[0];
			$post['filehash'] = $hash;
		} elseif ($config['max_images'] === 1) {
			$post['filehash'] = md5_file($file['tmp_name']);
		} else {
			$str_to_hash = '';
			foreach (explode(' ', $filenames) as $i => $f) {
				$str_to_hash .= file_get_contents($f);
			}
			$post['filehash'] = md5($str_to_hash);
		}
	}
	
	if (!hasPermission($config['mod']['bypass_filters'], $board['uri'])) {
		require_once 'inc/filters.php';	
		
		do_filters($post);
	}
	
	if ($post['has_file']) {	
		foreach ($post['files'] as $key => &$file) {
		if ($file['is_an_image'] && $config['ie_mime_type_detection'] !== false) {
			// Check IE MIME type detection XSS exploit
			$buffer = file_get_contents($file['tmp_name'], null, null, null, 255);
			if (preg_match($config['ie_mime_type_detection'], $buffer)) {
				undoImage($post);
				error($config['error']['mime_exploit']);
			}
			// exec("ffprobe -v quiet -print_format json -show_streams " . escapeshellarg($file['tmp_name']), $output);
				// $ffprobe = json_decode(implode("\n", $output), 1);
				// $file['width'] = $ffprobe['streams'][0]['width'];
				// $file['height'] = $ffprobe['streams'][0]['height'];
				// exec("identify -format %wx%h " . escapeshellarg($file['tmp_name']) . '[0]', $output);
				// $explodedOutput = explode('x', $output[0]);
				// $file['width'] = $explodedOutput[0];
				// $file['height'] = $explodedOutput[1];
				$size = @getimagesize($file['tmp_name']);
				$file['width'] = $size[0];
				$file['height'] = $size[1];
			// require_once 'inc/image.php';
			
			// find dimensions of an image using GD
			// if (!$size = @getimagesize($file['tmp_name'])) {
				// error($config['error']['invalidimg']);
			// }
			// if ($size[0] > $config['max_width'] || $size[1] > $config['max_height']) {
				// error($config['error']['maxsize']);
			// }
			
			// create image object
			// $image = new Image($file['tmp_name'], $file['extension'], $size);
			// if ($image->size->width > $config['max_width'] || $image->size->height > $config['max_height']) {
				// $image->delete();
				// error($config['error']['maxsize']);
			// }
			
			// $file['width'] = $image->size->width;
			// $file['height'] = $image->size->height;
			
			if ($config['spoiler_images'] && isset($_POST['spoiler'])) {
				$file['thumb'] = 'spoiler';
				
				$size = @getimagesize($config['spoiler_image']);
				$file['thumbwidth'] = $size[0];
				$file['thumbheight'] = $size[1];
			} else {
				// if ($imagick->getImageAlphaChannel() == 0)
					// $hasAlpha = false;
				// else
					// $hasAlpha = true;
					// if ($ffprobe['streams'][0]['pix_fmt'] == 'rgba' || $ffprobe['streams'][0]['pix_fmt'] == 'bgra')
						// $hasAlpha = true;
					// else
						// $hasAlpha = false;
				$file['thumb'] = $config['dir']['thumb'] . $file['file_id'] . ($file['extension'] != "jpg" ? ".png" : '.jpg');
				// $imagick->thumbnailImage($post['op'] ? $config['thumb_op_width'] : $config['thumb_width'], $post['op'] ? $config['thumb_op_height'] : $config['thumb_height']);
				// $imagick->writeImage($file['thumb']);
				// if ($error = shell_exec_error("ffmpeg -i " . escapeshellarg($file['tmp_name']) . " -v error -vframes 1 -pix_fmt yuvj420p -filter_complex 'scale=iw*min(1\,min(" . escapeshellarg($post['op'] ? $config['thumb_op_width'] : $config['thumb_width']) . "/iw\," . escapeshellarg($post['op'] ? $config['thumb_op_height'] : $config['thumb_height']) . "/ih)):-1' -y " . escapeshellarg($file['thumb'])))
					// error($error);
				if ($error = shell_exec_error("convert -quiet " . escapeshellarg($file['tmp_name']) . "[0]" . " -quality 75 -thumbnail " . escapeshellarg($post['op'] ? $config['thumb_op_width'] : $config['thumb_width']) . "x" . escapeshellarg($post['op'] ? $config['thumb_op_height'] : $config['thumb_height']) . "\> " . escapeshellarg($file['thumb'])))
					error($error);
				// if ($error = shell_exec_error("vipsthumbnail " . $file['tmp_name'] . " --interpolator bicubic --size " . escapeshellarg($post['op'] ? $config['thumb_op_width'] : $config['thumb_width']) . 'x' . escapeshellarg($post['op'] ? $config['thumb_op_height'] : $config['thumb_height']) . " -o " . escapeshellarg($file['thumb'])))
					// error($error);
				// exec("ffprobe -v quiet -print_format json -show_streams " . escapeshellarg($file['thumb']), $output_thumb);
					// $ffprobe_thumb = json_decode(implode("\n", $output_thumb), 1);
					// $file['thumbwidth'] = $ffprobe_thumb['streams'][0]['width'];
					// $file['thumbheight'] = $ffprobe_thumb['streams'][0]['height'];
				$size = @getimagesize($file['thumb']);
				$file['thumbwidth'] = $size[0];
				$file['thumbheight'] = $size[1];
				if ($config['use_jpegtran'] && $file['extension'] == "jpg") {
					if ($error = shell_exec_error('/opt/mozjpeg/bin/jpegtran -copy none -outfile ' .
						escapeshellarg($file['thumb']) . ' ' . escapeshellarg($file['thumb'])))
						error(_('Could not optimize jpg thumbnail!'), null, $error);
				}
				if ($file['extension'] != "jpg" && $config['use_optipng']) {
					if ($error = shell_exec_error('pngquant -f --quality=65-80 --speed 1 --skip-if-larger ' . escapeshellarg($file['thumb']) . ' -o ' . escapeshellarg($file['thumb'])))
						error($error);
					if ($error = shell_exec_error('optipng -quiet -strip all ' . escapeshellarg($file['thumb'])))
						error($error);
				}
				// if ($file['extension'] == 'gif' && $config['use_gifsicle'] && $config['thumb_method'] !== 'convert+gifsicle' && $config['thumb_method'] !== 'gm+gifsicle') {
					// if($error = shell_exec_error('gifsicle -b -w -O3 ' . escapeshellarg($file['thumb'])))
						// error(_('Could not optimize thumbnail!'), null, $error);
				// }
			}
			
			if ($file['extension'] == 'jpg' && $config['use_jpegtran']) {
				if ($error = shell_exec_error('/opt/mozjpeg/bin/jpegtran -copy none -outfile ' .
					escapeshellarg($file['tmp_name']) . ' ' . escapeshellarg($file['tmp_name'])))
					error(_('Could not strip EXIF metadata!'), null, $error);
			}
			if ($file['extension'] == 'png' && $config['use_optipng']) 
				exec('optipng -quiet -o1 -strip all ' . escapeshellarg($file['tmp_name']));
			// if ($file['extension'] == 'gif' && $config['use_gifsicle']) {
				// if ($error = shell_exec_error('gifsicle -b -w -O3 ' . escapeshellarg($file['tmp_name'])))
					// error(_('Could not optimize gif file!'), null, $error);
			// }
			// $gmagick->clear();
		} 
		// elseif ($file['extension'] == 'mp3') {
			// $file['thumb'] = $config['dir']['thumb'] . $file['file_id'] . '.jpg';
			// error($file['thumb']);
			// exec("ffmpeg -i " . escapeshellarg($file['tmp_name']) . " -v quiet -an -vframes 1 -f mjpeg -qscale:v 5 -vf scale=" . escapeshellarg($post['op'] ? $config['thumb_op_width'] : $config['thumb_width']) . ":" . escapeshellarg($post['op'] ? $config['thumb_op_height'] : $config['thumb_height']) . " " . escapeshellarg($file['thumb']));
			// $size = @getimagesize($file['thumb']);
			// $file['thumbwidth'] = $size[0];
			// $file['thumbheight'] = $size[1];
			// if ($config['use_jpegtran']) {
				// if ($error = shell_exec_error('/opt/libmozjpeg/bin/jpegtran -copy none -outfile ' .
					// escapeshellarg($file['thumb']) . ' ' . escapeshellarg($file['thumb'])))
					// error(_('Could not optimize jpg thumbnail!'), null, $error);
			// }
		// }
		elseif ($file['is_an_audio']) {
			// if ($file['extension'] == 'mp3') {
				// if ($error = shell_exec_error("ffmpeg -i " . escapeshellarg($file['tmp_name']) . " -c copy -write_id3v2 1 -id3v2_version 3 -v error -y " . escapeshellarg($file['tmp_name']) . "." . $file['extension']))
					// error($error);
			// }
			rename($file['tmp_name'] . "." . $file['extension'], $file['tmp_name']);
			require_once('inc/lib/getid3/getid3.php');
			$getID3 = new getID3;
			$ThisFileInfo = $getID3->analyze($file['tmp_name']);
			getid3_lib::CopyTagsToComments($ThisFileInfo);
			if (isset($ThisFileInfo['comments']['picture'][0])) {
				$file['thumb'] = $config['dir']['thumb'] . $file['file_id'] . ($ThisFileInfo['comments']['picture'][0]['image_mime'] == 'image/jpeg' ? '.jpg' : '.png');
				file_put_contents($file['thumb'], $ThisFileInfo['comments']['picture'][0]['data']);
				if ($error = shell_exec_error("convert -quiet " . escapeshellarg($file['thumb']) . "[0]" . " -quality 75 -thumbnail " . escapeshellarg($post['op'] ? $config['thumb_op_width'] : $config['thumb_width']) . "x" . escapeshellarg($post['op'] ? $config['thumb_op_height'] : $config['thumb_height']) . " " . escapeshellarg($file['thumb'])))
					error($error);
				$size = @getimagesize($file['thumb']);
				$file['thumbwidth'] = $size[0];
				$file['thumbheight'] = $size[1];
				if ($config['use_jpegtran'] && $ThisFileInfo['comments']['picture'][0]['image_mime'] == 'image/jpeg') {
					if ($error = shell_exec_error('/opt/mozjpeg/bin/jpegtran -copy none -outfile ' .
						escapeshellarg($file['thumb']) . ' ' . escapeshellarg($file['thumb'])))
						error(_('Could not optimize jpg thumbnail!'), null, $error);
				}
				if ($config['use_optipng'] && $ThisFileInfo['comments']['picture'][0]['image_mime'] != 'image/jpeg') {
					if ($error = shell_exec_error('pngquant -f --quality=65-80 --speed 1 --skip-if-larger ' . escapeshellarg($file['thumb']) . ' -o ' . escapeshellarg($file['thumb'])))
						error($error);
					if ($error = shell_exec_error('optipng -quiet -strip all ' . escapeshellarg($file['thumb'])))
						error($error);
				}
			}
			else {
				$file['thumb'] = 'file';

				$size = @getimagesize(sprintf($config['file_thumb'],
					isset($config['file_icons'][$file['extension']]) ?
						$config['file_icons'][$file['extension']] : $config['file_icons']['default']));
				$file['thumbwidth'] = $size[0];
				$file['thumbheight'] = $size[1];
			}
			if ($ThisFileInfo['playtime_string'])
				$file['duration'] = $ThisFileInfo['playtime_string'];
			if ($ThisFileInfo['comments_html']['title'][0])
				$file['title'] = $ThisFileInfo['comments_html']['title'][0];
			if ($ThisFileInfo['comments_html']['artist'][0])
				$file['artist'] = $ThisFileInfo['comments_html']['artist'][0];
			if ($ThisFileInfo['comments_html']['artist'][0])
				$file['album'] = $ThisFileInfo['comments_html']['album'][0];
			if ($ThisFileInfo['comments_html']['date'][0])
				$file['year'] = $ThisFileInfo['comments_html']['date'][0];
			if ($ThisFileInfo['comments_html']['creation_date'][0])
				$file['year'] = $ThisFileInfo['comments_html']['creation_date'][0];
			if ($ThisFileInfo['comments_html']['year'][0])
				$file['year'] = $ThisFileInfo['comments_html']['year'][0];
			if ($ThisFileInfo['comments_html']['release_time'][0])
				$file['year'] = $ThisFileInfo['comments_html']['release_time'][0];
		}
		elseif ($file['is_a_video']) {
			if ($error = shell_exec_error("ffmpeg -i " . escapeshellarg($file['tmp_name']) . " -c copy -movflags +faststart -v error -y " . escapeshellarg($file['tmp_name']) . "." . $file['extension']))
				error($error);
			rename($file['tmp_name'] . "." . $file['extension'], $file['tmp_name']);
			$file['thumb'] = $config['dir']['thumb'] . $file['file_id'] . '.jpg';
			exec("ffprobe -v quiet -print_format json -show_format -show_streams " . escapeshellarg($file['tmp_name']), $ffprobe);
			$ffprobe = json_decode(implode("\n", $ffprobe), 1);
			$file['width'] = $ffprobe['streams'][0]['width'];
			$file['height'] = $ffprobe['streams'][0]['height'];
			if ($ffprobe['format']['duration'])
				$file['duration'] = gmdate("i:s", $ffprobe['format']['duration']);
			if ($error = shell_exec_error("ffmpeg -i " . escapeshellarg($file['tmp_name']) . " -v error -an -vframes 1 -f mjpeg -qscale:v 5 -filter_complex 'scale=iw*min(1\,min(" .
				escapeshellarg($post['op'] ? $config['thumb_op_width'] : $config['thumb_width']) . "/iw\," . escapeshellarg($post['op'] ? $config['thumb_op_height'] : $config['thumb_height']) . "/ih)):-1' -y " . escapeshellarg($file['thumb'])))
				error($error);
			$size = @getimagesize($file['thumb']);
			$file['thumbwidth'] = $size[0];
			$file['thumbheight'] = $size[1];
			if ($config['use_jpegtran']) {
				if ($error = shell_exec_error('/opt/mozjpeg/bin/jpegtran -copy none -outfile ' .
					escapeshellarg($file['thumb']) . ' ' . escapeshellarg($file['thumb'])))
					error(_('Could not optimize jpg thumbnail!'), null, $error);
			}
		}
		else {
			// not an image
			//copy($config['file_thumb'], $post['thumb']);
			$file['thumb'] = 'file';

			$size = @getimagesize(sprintf($config['file_thumb'],
				isset($config['file_icons'][$file['extension']]) ?
					$config['file_icons'][$file['extension']] : $config['file_icons']['default']));
			$file['thumbwidth'] = $size[0];
			$file['thumbheight'] = $size[1];
		}
		
		if (!isset($dont_copy_file) || !$dont_copy_file) {
			if (isset($file['file_tmp'])) {
				if (!@rename($file['tmp_name'], $file['file']))
					error($config['error']['nomove']);
				chmod($file['file'], 0644);
			} elseif (!@move_uploaded_file($file['tmp_name'], $file['file']))
				error($config['error']['nomove']);
			}
		}

		if ($config['image_reject_repost']) {
			if ($p = getPostByHash($post['filehash'])) {
				undoImage($post);
				error(sprintf($config['error']['fileexists'], 
					($post['mod'] ? $config['root'] . $config['file_mod'] . '?/' : $config['root']) .
					($board['dir'] . $config['dir']['res'] .
						($p['thread'] ?
							$p['thread'] . '.html#' . $p['id']
						:
							$p['id'] . '.html'
						))
				));
			}
		} else if (!$post['op'] && $config['image_reject_repost_in_thread']) {
			if ($p = getPostByHashInThread($post['filehash'], $post['thread'])) {
				undoImage($post);
				error(sprintf($config['error']['fileexistsinthread'], 
					($post['mod'] ? $config['root'] . $config['file_mod'] . '?/' : $config['root']) .
					($board['dir'] . $config['dir']['res'] .
						($p['thread'] ?
							$p['thread'] . '.html#' . $p['id']
						:
							$p['id'] . '.html'
						))
				));
			}
		}
		// $file['size'] = filesize($file['tmp_name']);
		}
	
	if (!hasPermission($config['mod']['postunoriginal'], $board['uri']) && $config['robot_enable'] && checkRobot($post['body_nomarkup'])) {
		undoImage($post);
		if ($config['robot_mute']) {
			error(sprintf($config['error']['muted'], mute()));
		} else {
			error($config['error']['unoriginal']);
		}
	}
	
	// Remove board directories before inserting them into the database.
	if ($post['has_file']) {
		foreach ($post['files'] as $key => &$file) {
			$file['file_path'] = $file['file'];
			$file['thumb_path'] = $file['thumb'];
			$file['file'] = mb_substr($file['file'], mb_strlen($config['dir']['img']));
			if (($file['is_an_image'] || $file['is_an_audio'] || $file['is_a_video']) && $file['thumb'] != 'spoiler' && $file['thumb'] != 'file')
				$file['thumb'] = mb_substr($file['thumb'], mb_strlen($config['dir']['thumb']));
		}
	}
	
	$post = (object)$post;
	$post->files = array_map(function($a) { return (object)$a; }, $post->files);
	$error = event('post', $post);
	$post->files = array_map(function($a) { return (array)$a; }, $post->files);

	if ($error) {
		undoImage((array)$post);
		error($error);
	}
	$post = (array)$post;

	if ($post['files'])
		$post['files'] = $post['files'];
	$post['num_files'] = sizeof($post['files']);
	
	$post['id'] = $id = post($post);
	
	insertFloodPost($post);
	
	if (isset($post['antispam_hash'])) {
		incrementSpamHash($post['antispam_hash']);
	}
	
	if (isset($post['tracked_cites']) && !empty($post['tracked_cites'])) {
		$insert_rows = array();
		foreach ($post['tracked_cites'] as $cite) {
			$insert_rows[] = '(' .
				$pdo->quote($board['uri']) . ', ' . (int)$id . ', ' .
				$pdo->quote($cite[0]) . ', ' . (int)$cite[1] . ')';
		}
		query('INSERT INTO ``cites`` VALUES ' . implode(', ', $insert_rows)) or error(db_error());
	}
	
	if (!$post['op'] && strtolower($post['email']) != 'sage' && !$thread['sage'] && ($config['reply_limit'] == 0 || $numposts['replies']+1 < $config['reply_limit'])) {
		bumpThread($post['thread']);
	}
	
	buildThread($post['op'] ? $id : $post['thread']);
	
	if ($config['try_smarter'] && $post['op'])
		$build_pages = range(1, $config['max_pages']);
	
	if ($post['op'])
		clean();
	
	event('post-after', $post);
	
	buildIndex();
	
	if (isset($_SERVER['HTTP_REFERER'])) {
		// Tell Javascript that we posted successfully
		if (isset($_COOKIE[$config['cookies']['js']]))
			$js = json_decode($_COOKIE[$config['cookies']['js']]);
		else
			$js = (object) array();
		// Tell it to delete the cached post for referer
		$js->{$_SERVER['HTTP_REFERER']} = true;
		// Encode and set cookie
		setcookie($config['cookies']['js'], json_encode($js), 0, $config['cookies']['jail'] ? $config['cookies']['path'] : '/', null, false, false);
	}
	
	$root = $post['mod'] ? $config['root'] . $config['file_mod'] . '?/' : $config['root'];
	
	if ($noko) {
		$redirect = $root . $board['dir'] . $config['dir']['res'] .
			sprintf($config['link_page'], $post['op'] ? $id:$post['thread']) . (!$post['op'] ? '#' . $id : '');
	   	
		if (!$post['op'] && isset($_SERVER['HTTP_REFERER'])) {
			$regex = array(
				'board' => str_replace('%s', '(\w{1,8})', preg_quote($config['board_path'], '/')),
				'page' => str_replace('%d', '(\d+)', preg_quote($config['file_page'], '/')),
				'page50' => str_replace('%d', '(\d+)', preg_quote($config['file_page50'], '/')),
				'res' => preg_quote($config['dir']['res'], '/'),
			);

			if (preg_match('/\/' . $regex['board'] . $regex['res'] . $regex['page50'] . '([?&].*)?$/', $_SERVER['HTTP_REFERER'])) {
				$redirect = $root . $board['dir'] . $config['dir']['res'] .
					sprintf($config['file_page50'], $post['op'] ? $id:$post['thread']) . (!$post['op'] ? '#' . $id : '');
			}
		}
	} else {
		$redirect = $root . $board['dir'] . $config['link_index'];
		
	}
	
	if ($config['syslog'])
		_syslog(LOG_INFO, 'New post: /' . $board['dir'] . $config['dir']['res'] .
			sprintf($config['file_page'], $post['op'] ? $id : $post['thread']) . (!$post['op'] ? '#' . $id : ''));
	
	if (!$post['mod']) header('X-Associated-Content: "' . $redirect . '"');

	if ($post['op'])
		rebuildThemes('post-thread', $board['uri']);
	else
		rebuildThemes('post', $board['uri']);
	
	if (!isset($_POST['json_response'])) {
		header('Location: ' . $redirect, true, $config['redirect_http']);
	} else {
		header('Content-Type: text/json; charset=utf-8');
		echo json_encode(array(
			'redirect' => $redirect,
			'noko' => $noko,
			'id' => $id
		));
	}
} elseif (isset($_POST['appeal'])) {
	if (!isset($_POST['ban_id']))
		error($config['error']['bot']);
	
	$ban_id = (int)$_POST['ban_id'];
	
	$bans = Bans::find($_SERVER['REMOTE_ADDR']);
	foreach ($bans as $_ban) {
		if ($_ban['id'] == $ban_id) {
			$ban = $_ban;
			break;
		}
	}
	
	if (!isset($ban)) {
		error(_("That ban doesn't exist or is not for you."));
	}
	
	if ($ban['expires'] && $ban['expires'] - $ban['created'] <= $config['ban_appeals_min_length']) {
		error(_("You cannot appeal a ban of this length."));
	}
	
	$query = query("SELECT `denied` FROM ``ban_appeals`` WHERE `ban_id` = $ban_id") or error(db_error());
	$ban_appeals = $query->fetchAll(PDO::FETCH_COLUMN);
	
	if (count($ban_appeals) >= $config['ban_appeals_max']) {
		error(_("You cannot appeal this ban again."));
	}
	
	foreach ($ban_appeals as $is_denied) {
		if (!$is_denied)
			error(_("There is already a pending appeal for this ban."));
	}
	
	$query = prepare("INSERT INTO ``ban_appeals`` VALUES (NULL, :ban_id, :time, :message, 0)");
	$query->bindValue(':ban_id', $ban_id, PDO::PARAM_INT);
	$query->bindValue(':time', time(), PDO::PARAM_INT);
	$query->bindValue(':message', $_POST['appeal']);
	$query->execute() or error(db_error($query));
	
	displayBan($ban);
} else {
	if (!file_exists($config['has_installed'])) {
		header('Location: install.php', true, $config['redirect_http']);
	} else {
		// They opened post.php in their browser manually.
		error($config['error']['nopost']);
	}
}
